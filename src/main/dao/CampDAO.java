package main.dao;

import main.domain.Camp;
import main.domain.SportsBranch;
import main.domain.SportsCenter;
import main.utilities.ConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CampDAO {
    private Connection connection;

    public CampDAO(ConnectionManager connectionManager) throws SQLException {
        this.connection = connectionManager.getConnection();
    }

    public Camp findCampByID(int campId) throws SQLException {
        Camp camp;
        PreparedStatement statement = connection.prepareStatement("SELECT * from camp ca inner join sportsbranch br on ca.sportsbranch_id = br.id inner join sportscenter sc on ca.center_id = sc.id where ca.id = ?");
        statement.setInt(1, campId);
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            camp = setToData(set);
        } else {
            throw new SQLException("no records were found.");
        }
        return camp;
    }

    public List<Camp> findCampByName(String name) throws SQLException {
        List<Camp> camps = new ArrayList<>();
        PreparedStatement statement = connection.prepareStatement("SELECT * from camp ca inner join sportsbranch br on ca.sportsbranch_id = br.id inner join sportscenter sc on ca.center_id = sc.id where ca.campname like ?");
        statement.setString(1, "%" + name + "%");
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                Camp camp = setToData(set);
                camps.add(camp);
            } while (set.next());
        } else {
            throw new SQLException("no records found");
        }
        return camps;
    }

    private Camp setToData(ResultSet set) throws SQLException {
        return new Camp(set.getInt("ca.id"),
                new SportsCenter(
                        set.getInt("sc.id"),
                        set.getString("sc.centername"),
                        set.getString("sc.street"),
                        set.getString("sc.housenumber"),
                        set.getInt("sc.postalcode"),
                        set.getString("sc.city"),
                        set.getString("sc.phone")),
                set.getString("ca.campname"),
                set.getDate("ca.startdate"),
                set.getDate("ca.enddate"),
                new SportsBranch(set.getInt("br.id"),
                        set.getString("br.omschrijving")),
                set.getInt("ca.min_birthyear"),
                set.getInt("ca.max_birthyear"),
                set.getDouble("ca.price"),
                set.getInt("ca.available_spots"));
    }
}
