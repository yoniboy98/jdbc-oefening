package main.dao;


import main.domain.SportsCenter;
import main.utilities.ConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SportCenterDAO {
    private Connection connection;


    public SportCenterDAO(ConnectionManager connectionManager) throws SQLException {
        this.connection = connectionManager.getConnection();
    }

    public SportsCenter findSportsCenterByID(int id) throws SQLException {
        SportsCenter sportsCenter;
        PreparedStatement statement = connection.prepareStatement("Select * from sportscenter where id= ?");
        statement.setInt(1, id);
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            sportsCenter = setToData(set);
        } else {
            throw new SQLException("no records were found.");
        }
        return sportsCenter;
    }

    public List<SportsCenter> findSportCenters() throws SQLException {
        List<SportsCenter> sportsCenters = new ArrayList<>();
        PreparedStatement statement = connection.prepareStatement("SELECT * from sportscenter");
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                sportsCenters.add(setToData(set));
            } while (set.next());
        } else {
            throw new SQLException("no records were found");
        }
        return sportsCenters;
    }

    public List<SportsCenter> findSportCentersOnName(String name) throws SQLException {
        List<SportsCenter> sportsCenters = new ArrayList<>();
        PreparedStatement statement = connection.prepareStatement("SELECT * from sportscenter WHERE centername Like ?");
        statement.setString(1,"%" + name + "%");
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            do {
                sportsCenters.add(setToData(set));
            } while (set.next());
        } else {
            throw new SQLException("no records were found");
        }
        return sportsCenters;
    }


    private SportsCenter setToData(ResultSet set) throws SQLException {
        return new SportsCenter(set.getInt("id"), set.getString("centername"), set.getString("street"), set.getString("housenumber"), set.getInt("postalcode"), set.getString("city"), set.getString("phone"));
    }
}
