package main.domain;

public class SportsCenter {
   private String phoneNumber;
   private int postalCode;
   private String city;
   private String houseNumber;
   private String street;
   private int id;
   private String centerName;

    public SportsCenter() {
        this.phoneNumber = "default";
        this.postalCode = 0;
        this.id = 0;
        this.centerName = "default";
        this.city = "default";
        this.street =  "default";
        this.houseNumber = "default";
    }

    public SportsCenter(int id, String centerName, String street, String houseNumber, int postalCode, String city, String phoneNumber) {
        this.phoneNumber = phoneNumber;
        this.postalCode = postalCode;
        this.city = city;
        this.houseNumber = houseNumber;
        this.street = street;
        this.id = id;
        this.centerName = centerName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(int postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCenterName() {
        return centerName;
    }

    public void setCenterName(String centerName) {
        this.centerName = centerName;
    }

    @Override
    public String toString() {
        return "SportsCenter{" +
                "phoneNumber='" + phoneNumber + '\'' +
                ", postalCode=" + postalCode +
                ", city='" + city + '\'' +
                ", houseNumber='" + houseNumber + '\'' +
                ", street='" + street + '\'' +
                ", id=" + id +
                ", centerName='" + centerName + '\'' +
                '}';
    }
}
