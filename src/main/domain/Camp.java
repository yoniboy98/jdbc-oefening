package main.domain;

import java.sql.Date;

public class Camp {
    int id;
    SportsCenter sportsCenter;
    String campName;
    Date startDate;
    Date endDate;
    SportsBranch sportsBranch;
    int minBirthYear;
    int maxBirthYear;
    double price;
    int availableSpots;

    public Camp(int id, SportsCenter sportsCenter, String campName, Date startDate, Date endDate, SportsBranch sportsBranch, int minBirthYear, int maxBirthYear, double price, int availableSpots) {
        this.id = id;
        this.sportsCenter = sportsCenter;
        this.campName = campName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.sportsBranch = sportsBranch;
        this.minBirthYear = minBirthYear;
        this.maxBirthYear = maxBirthYear;
        this.price = price;
        this.availableSpots = availableSpots;
    }
    public Camp() {
        this.id = 0;
        this.sportsCenter = new SportsCenter();
        this.campName = "campName";
        this.startDate = new Date(0L);
        this.endDate = new Date(0L);
        this.sportsBranch = new SportsBranch();
        this.minBirthYear = 0;
        this.maxBirthYear = 0;
        this.price = 0.0;
        this.availableSpots = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SportsCenter getSportsCenter() {
        return sportsCenter;
    }

    public void setSportsCenter(SportsCenter sportsCenter) {
        this.sportsCenter = sportsCenter;
    }

    public String getCampName() {
        return campName;
    }

    public void setCampName(String campName) {
        this.campName = campName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public SportsBranch getSportsBranch() {
        return sportsBranch;
    }

    public void setSportsBranch(SportsBranch sportsBranch) {
        this.sportsBranch = sportsBranch;
    }

    public int getMinBirthYear() {
        return minBirthYear;
    }

    public void setMinBirthYear(int minBirthYear) {
        this.minBirthYear = minBirthYear;
    }

    public int getMaxBirthYear() {
        return maxBirthYear;
    }

    public void setMaxBirthYear(int maxBirthYear) {
        this.maxBirthYear = maxBirthYear;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAvailableSpots() {
        return availableSpots;
    }

    public void setAvailableSpots(int availableSpots) {
        this.availableSpots = availableSpots;
    }

    @Override
    public String toString() {
        return "Camp{" +
                "id=" + id +
                ", sportsCenter=" + sportsCenter +
                ", campName='" + campName + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", sportsBranch=" + sportsBranch +
                ", minBirthYear=" + minBirthYear +
                ", maxBirthYear=" + maxBirthYear +
                ", price=" + price +
                ", availableSpots=" + availableSpots +
                '}';
    }
}
