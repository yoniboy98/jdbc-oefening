package main.domain;

public class SportsBranch {
    int id;
    String omschrijving;

    public SportsBranch(int id, String omschrijving) {
        this.id = id;
        this.omschrijving = omschrijving;
    }

    public SportsBranch() {
        this.id = 0;
        this.omschrijving = "default";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOmschrijving() {
        return omschrijving;
    }

    public void setOmschrijving(String omschrijving) {
        this.omschrijving = omschrijving;
    }

    @Override
    public String toString() {
        return "SportsBranch{" +
                "id=" + id +
                ", omschrijving='" + omschrijving + '\'' +
                '}';
    }
}
