package main.application;

import main.dao.CampDAO;
import main.dao.SportCenterDAO;
import main.domain.Camp;
import main.domain.SportsCenter;
import main.utilities.ConnectionManager;
import main.utilities.MySQLConnectionManager;

import java.sql.SQLException;
import java.util.List;

public class runnerTemp {
    public static void main(String[] args) {
        ConnectionManager connectionManager = null;
        try {
            connectionManager = new MySQLConnectionManager("root", "");
            SportCenterDAO sportCenterDAO = new SportCenterDAO(connectionManager);
            SportsCenter sportsCenter = sportCenterDAO.findSportsCenterByID(1);
            System.out.println(sportsCenter.toString());
            List<SportsCenter> sportsCenters = sportCenterDAO.findSportCentersOnName("e");
            for (SportsCenter center : sportsCenters) {
                System.out.println(center.toString());
            }
            CampDAO campDAO = new CampDAO(connectionManager);
            Camp camp = campDAO.findCampByID(1);
            List<Camp> camps = campDAO.findCampByName("e");
            System.out.println(camp.toString());
            for (Camp campfromlist : camps) {
                System.out.println(campfromlist.toString());
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connectionManager != null) {
                connectionManager.closeConnection();
            }
        }
    }
}

